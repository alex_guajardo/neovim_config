require("mason").setup()
require("mason-lspconfig").setup({
  ensure_installed = {
    "lua_ls",
    "ruby_ls",
    "sqlls",
    "bashls",
    "clangd",
    "dockerls",
    "docker_compose_language_service",
    "eslint",
    "elixirls",
    "html",
    "jsonls",
    "quick_lint_js",
    "marksman",
    "spectral",
    "intelephense",
    "jedi_language_server",
    "volar",
    "lemminx",
    "yamlls",
  }
})

local capabilities = require('cmp_nvim_lsp').default_capabilities()

require("lspconfig").lua_ls.setup {
  capabilities = capabilities
}
require("lspconfig").ruby_ls.setup {
  capabilities = capabilities
}
